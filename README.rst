*Note: This is an attempt at re-imagining the way we build and distribute
Ideascube. It is an experiment, and might lead nowhere. Do not rely on anything
here.*

org.ideascube.BaseSdk
=====================

This repository contains the required tools to build the base of the SDK we use
to develop Ideascube applications.

Building the SDK
----------------

To build the SDK, simply run::

    $ ./build-base-sdk.sh TARGET_DIR ARCHITECTURE

Your `sudo` password will be requested at some point.

The architecture must be one of:

* arm
* x86_64

Using the base SDK
------------------

*Note: You probably don't want to use this base SDK. Instead, you should use
the more complete `Ideascube SDK`_.*

The actual base SDK is published in an `OSTree repository`_. Check out the
``org.ideascube.BaseSdk/${ARCH}/${VERSION}`` ref to get it.

We currently only support the ``arm`` and ``x86_64`` architectures, and there
is only a ``devel`` version.


.. _Ideascube SDK: https://framagit.org/bochecha/org.ideascube.Sdk
.. _OSTree repository: https://web1.kymeria.6clones.net/ideascube/repo-base-sdk
