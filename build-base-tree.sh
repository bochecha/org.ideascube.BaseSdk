#!/bin/bash


# This script builds a minimal / tree out of the Fedora packages, as the
# basis for our SDK.


set -o errexit
set -o nounset
set -o pipefail


SDK=$1
INSTALLROOT=$(pwd)/${SDK}
ARCH=$2

if [[ ${ARCH} == "arm" ]]; then
    # That's how Debian calls the ARM 32 architecture
    ARCH="armhf"
elif [[ ${ARCH} == "x86_64" ]]; then
    # That's how Debian calls the x86 64 architecture
    ARCH="amd64"
fi

# TODO: Cache downloaded packages
# TODO: Don't install so many things
DEBSTP_ARGS="--arch=${ARCH} --merged-usr --variant=minbase stretch ${INSTALLROOT}"
PACKAGES="autoconf,automake,bash,findutils,gawk,gcc,libbz2-dev,libexpat1-dev,libffi-dev,liblzma-dev,libreadline-dev,libsqlite3-dev,libssl-dev,libtool,make,pkg-config,zlib1g-dev"
EXCLUDE="apt,logrotate,nano,rsyslog,sensible-utils,systemd,systemd-sysv,tasksel,tasksel-data,udev,whiptail"


function empty_directory {
    path=$1
    rm -fr ${path}
    mkdir -p ${path}
}

function keep_only_in {
    directory=$1; shift
    keep=$@

    mv ${directory} ${directory}.bak
    mkdir ${directory}

    for name in ${keep}; do
        mv ${directory}.bak/${name} ${directory}/
    done

    rm -fr ${directory}.bak
}


# Build a base tree
sudo /usr/sbin/debootstrap --include=${PACKAGES} --exclude=${EXCLUDE} ${DEBSTP_ARGS}

# Make the tree available to the user
sudo /bin/chown -R ${USER}:${USER} ${INSTALLROOT}

# Get rid of alternatives
ALL_LINKS=$(find ${INSTALLROOT}/usr -type l)
for DST in ${ALL_LINKS}; do
    LINKED=$(readlink ${DST})

    if [ "${LINKED:0:18}" == "/etc/alternatives/" ]; then
        SRC=$(readlink ${INSTALLROOT}${LINKED})
        ln -sf ${SRC} ${DST}
    fi
done

# Empty some directories
empty_directory ${INSTALLROOT}/dev
empty_directory ${INSTALLROOT}/proc
empty_directory ${INSTALLROOT}/run
empty_directory ${INSTALLROOT}/sys
empty_directory ${INSTALLROOT}/tmp

# Keep some minimal directories
keep_only_in ${INSTALLROOT}/etc debian_version ld.so.cache ld.so.conf ld.so.conf.d
keep_only_in ${INSTALLROOT}/var run

# Add a minimal /etc/hosts
cat > ${INSTALLROOT}/etc/hosts << EOF
127.0.0.1   localhost
::1         localhost
EOF

# Fixup some paths
rm -fr ${INSTALLROOT}/var/tmp && ln -s ../tmp ${INSTALLROOT}/var/tmp

# Clean up unnecessary stuff
rm -fr \
    ${INSTALLROOT}/boot \
    ${INSTALLROOT}/home \
    ${INSTALLROOT}/media \
    ${INSTALLROOT}/mnt \
    ${INSTALLROOT}/opt \
    ${INSTALLROOT}/root \
    ${INSTALLROOT}/srv \
    ${INSTALLROOT}/usr/bin/*gold \
    ${INSTALLROOT}/usr/bin/deb-* \
    ${INSTALLROOT}/usr/bin/debconf* \
    ${INSTALLROOT}/usr/bin/dpkg* \
    ${INSTALLROOT}/usr/bin/update-alternatives \
    ${INSTALLROOT}/usr/games \
    ${INSTALLROOT}/usr/lib/gold-ld \
    ${INSTALLROOT}/usr/lib/init \
    ${INSTALLROOT}/usr/lib/python2.7 \
    ${INSTALLROOT}/usr/lib/python3 \
    ${INSTALLROOT}/usr/lib/tmpfiles.d \
    ${INSTALLROOT}/usr/lib/udev \
    ${INSTALLROOT}/usr/lib/valgrind \
    ${INSTALLROOT}/usr/local \
    ${INSTALLROOT}/usr/share/debconf \
    ${INSTALLROOT}/usr/share/debianutils \
    ${INSTALLROOT}/usr/share/dpkg \
    ${INSTALLROOT}/usr/share/doc \
    ${INSTALLROOT}/usr/share/doc-base \
    ${INSTALLROOT}/usr/share/info \
    ${INSTALLROOT}/usr/share/lintian \
    ${INSTALLROOT}/usr/share/locale/*/LC_MESSAGES/*.mo \
    ${INSTALLROOT}/usr/share/man \
    ${INSTALLROOT}/usr/share/pixmaps \
    ${INSTALLROOT}/usr/src
